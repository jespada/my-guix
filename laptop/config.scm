;; my config

(use-modules (gnu)
             (nongnu packages linux))

(use-service-modules desktop networking ssh xorg)

(operating-system
  (kernel linux)
  (locale "en_GB.utf8")
  (timezone "Europe/London")
  (keyboard-layout (keyboard-layout "gb" "extd"))

;;  (firmware (cons* iwlwifi-firmware
;;                   %base-firmware))
  (firmware (append (list iwlwifi-firmware)
            %base-firmware))
  
  (bootloader
    (bootloader-configuration
      (bootloader grub-efi-bootloader)
      (target "/boot/efi")
      (keyboard-layout keyboard-layout)))
  (swap-devices (list "/dev/sda2"))
  (file-systems
    (cons* (file-system
             (mount-point "/boot/efi")
             (device (uuid "63D9-D471" 'fat32))
             (type "vfat"))
           (file-system
             (mount-point "/")
             (device
               (uuid "458b7d27-49de-4222-9153-971a975eb763"
                     'ext4))
             (type "ext4"))
           %base-file-systems))
  (host-name "thor")
  (users (cons* (user-account
                  (name "jespada")
                  (comment "Jorge Espada")
                  (group "users")
                  (home-directory "/home/jespada")
                  (supplementary-groups
                    '("wheel" "netdev" "audio" "video")))
                %base-user-accounts))
  (packages
    (append
      (list (specification->package "nss-certs"))
      %base-packages))
  (services
    (append
      (list (service gnome-desktop-service-type)
            (service openssh-service-type)
            (set-xorg-configuration
              (xorg-configuration
                (keyboard-layout keyboard-layout))))
      %desktop-services)))
